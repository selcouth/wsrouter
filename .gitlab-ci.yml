# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: python:3.10

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  POETRY_CACHE_DIR: "$CI_PROJECT_DIR/.cache/poetry"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/topics/caching/
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.

stages:
  - test
  - build
  - deploy

default:
  cache:
    paths:
      - .cache/poetry
      - .venv/

  before_script:
    - uname -a
    - python --version
      # Configure via poetry
    - pip install poetry
    - poetry config cache-dir $POETRY_CACHE_DIR
    - poetry config virtualenvs.in-project true
    - poetry install

test:
  stage: test
  variables:
    GIT_BRANCH: "$CI_BUILD_REF_NAME"
    COVERALLS_SERVICE_NAME: "gitlab-ci"
  script:
    - poetry run pytest --junitxml=report.xml --cov-report xml --cov wsrouter
    - poetry run coveralls
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'
  artifacts:
    when: always
    paths:
      - report.xml
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

build:
  stage: build
  needs: ["test"]
  script:
    - poetry install --with docs
    - poetry build
    - poetry run make -C docs html
  only:
    - main
    - tags
  artifacts:
    paths:
      - dist/*
      - docs/build/html

publish-gitlab:
  stage: deploy
  needs: ["build"]
  dependencies:
    - build
  when: manual
  retry: 2 # Because sometimes the CI_JOB_TOKEN variable creates problems with poetry
  script:
    - poetry config repositories.gitlab "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi"
    - poetry config http-basic.gitlab gitlab-ci-token "${CI_JOB_TOKEN}"
    - poetry publish --repository gitlab

publish-pypi:
  stage: deploy
  needs: ["build"]
  dependencies:
    - build
  when: manual
  only:
    - main
    - tags
  script:
    - poetry config pypi-token.pypi "${PYPI_UPLOAD_TOKEN}"
    - poetry publish

publish-pypi-test:
  stage: deploy
  needs: ["build"]
  dependencies:
    - build
  when: manual
  script:
    - poetry config repositories.pypi-test https://test.pypi.org/legacy/
    - poetry config pypi-token.pypi-test "${PYPI_TEST_UPLOAD_TOKEN}"
    - poetry publish --repository pypi-test

pages:
  stage: deploy
  needs: ["build"]
  script:
    - mv docs/build/html/ public/
  artifacts:
    paths:
      - public
  when: manual
