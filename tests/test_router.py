import asyncio
import logging
import orjson
import pathlib
import pytest
import shortuuid

from collections import namedtuple

from starlette.testclient import TestClient
from starlette.websockets import WebSocket
from starlette.routing import WebSocketRoute
from starlette.applications import Starlette

from wsrouter import WebSocketRouter
from wsrouter import WebSocketRouterEndpoint
from wsrouter import WebSocketCommandEndpont
from wsrouter import command

def test_javascript_path():
    assert isinstance(WebSocketRouter.JS_PATH, pathlib.Path)
    jsfile = WebSocketRouter.JS_PATH/'websocket.js'
    assert jsfile.exists()

def test_route_creation():
    class MyWSEndpoint(WebSocketCommandEndpont):
        pass

    wsroutes = {
        "/route1/path": MyWSEndpoint,
    }

    router = WebSocketRouter(routes=wsroutes)

    assert router.routes == wsroutes

    endpoint = router(scope={'type':'websocket'}, receive=None, send=None)

    assert isinstance(endpoint, WebSocketRouterEndpoint)

    assert endpoint.routes == wsroutes

def test_route_add():
    class MyWSEndpoint(WebSocketCommandEndpont):
        pass

    wsroutes = {
    }

    router = WebSocketRouter(routes=wsroutes)

    assert router.routes == wsroutes

    endpoint = router(scope={'type':'websocket'}, receive=None, send=None)

    assert isinstance(endpoint, WebSocketRouterEndpoint)

    assert endpoint.routes == {}

    new_route = {'route': 'new_route', 'endpoint': MyWSEndpoint}

    endpoint.add_route(**new_route)

    assert new_route['route'] in endpoint.routes
    assert endpoint.routes[new_route['route']] == new_route['endpoint']


def test_route_custom_endpoint():
    class MyEndpoint(WebSocketRouterEndpoint):
        def __init__(self, *arg, **kw):
            super().__init__(*arg, **kw)

    wsroutes = {
        "/ws/route/path": WebSocketCommandEndpont,
    }

    router = WebSocketRouter(routes=wsroutes, epClass=MyEndpoint)

    endpoint = router(scope={'type':'websocket'}, receive=None, send=None)

    assert isinstance(endpoint, MyEndpoint)

def test_route_override_default_endpoint():
    class MyWsEp(WebSocketRouterEndpoint):
        pass

    class MyWsRouter(WebSocketRouter):
        DEFAULT_ENDPOINT = MyWsEp

    wsroutes = {
        "/ws/route/path": WebSocketCommandEndpont,
    }

    router = MyWsRouter(routes=wsroutes)

    endpoint = router(scope={'type':'websocket'}, receive=None, send=None)

    assert isinstance(endpoint, MyWsEp)

@pytest.mark.asyncio
async def test_router_websocket_full(caplog):
    logging.getLogger().setLevel(logging.DEBUG)

    class TestWsEndpoint(WebSocketRouterEndpoint):
        def __init__(self, *arg, **kw):
            super().__init__(*arg, **kw)

        async def on_connect(self, websocket):
            await super().on_connect(websocket)
            assert self.websocket == websocket
            assert isinstance(self.websocket.state.routes, dict)
            self.websocket.state.wsep = self

        async def on_receive(self, websocket, data):
            msg = orjson.loads(data)
            await super().on_receive(websocket, data)

        async def on_disconnect(self, websocket, close_code):
            await super().on_disconnect(websocket, close_code)

    class TestCmdEndpoint(WebSocketCommandEndpont):
        called = 0

        @command("test_cmd")
        async def on_test_cmd(self):
            self.called += 1

    route = '/test/ws/route'

    wsroutes = {
        route: TestCmdEndpoint,
    }

    endpoint = WebSocketRouter(routes=wsroutes, epClass=TestWsEndpoint)

    assert endpoint.endpoint_class == TestWsEndpoint

    routes = [
        WebSocketRoute("/ws", endpoint = endpoint),
    ]

    app = Starlette(debug=True, routes=routes)

    client = TestClient(app)
    with client.websocket_connect('/ws') as websocket:
        websocket.send_json({'route':route, 'cmd':'test_cmd', 'data': {}})

        # Wait for the server code to process the command ...
        # TODO: Find a more deterministic way to handle this!
        await asyncio.sleep(0.1)

        assert 'routes' in websocket.scope['state']
        assert isinstance(websocket.scope['state']['routes'][route], TestCmdEndpoint)
        assert 'Command [test_cmd] received' in caplog.text

        websocket.send_json([])
        await asyncio.sleep(0.1)
        assert 'Unknown Message Format' in caplog.text

        websocket.send_json({'cmd':'test_cmd', 'data': {}})
        await asyncio.sleep(0.1)
        assert 'Message received with no route' in caplog.text

        websocket.send_json({'route':'/should_fail/', 'cmd':'test_cmd', 'data': {}})
        await asyncio.sleep(0.1)
        assert 'WebSocket message route has no endpoint' in caplog.text

        # Test disconnect command
        websocket.send_json({'route':route, 'cmd': 'disconnect', 'data': {}})
        await asyncio.sleep(0.1)
        assert route not in websocket.scope['state']['routes']
        assert len(websocket.scope['state']['routes']) == 0

        # Test disconnect of entire socket disconnects websocket routes
        websocket.send_json({'route':route, 'cmd':'test_cmd', 'data': {}})
        await asyncio.sleep(0.1)
        assert 'routes' in websocket.scope['state']
        assert isinstance(websocket.scope['state']['routes'][route], TestCmdEndpoint)
