import logging
import orjson
import pytest
import shortuuid

from collections import namedtuple

from wsrouter.cmd import command

from wsrouter import WebSocketCommandEndpont

class CmdTest(WebSocketCommandEndpont):
    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)
        self.called = {}
        self._logger.setLevel(logging.DEBUG)

    @command("foo")
    async def on_foo(self):
        self.called.setdefault('foo',0)
        self.called['foo'] += 1

    @command("fubar")
    @command("bar")
    async def on_bar(self):
        self.called.setdefault('bar',0)
        self.called['bar'] += 1

    async def on_nothing(self):
        self.called.setdefault('nothing',0)
        self.called['nothing'] += 1

    @command("arg_1")
    async def on_arg_1(self, arg1):
        self.called['arg_1'] = arg1

    @command("arg_2")
    async def on_arg_2(self, arg1, arg2):
        self.called['arg_2'] = (arg1, arg2)

    @command("arg_opt")
    async def on_arg_opt(self, arg1 = None):
        self.called['arg_opt'] = arg1

def test_message_setup():
    class ws:
        def __init__(self, uid):
            self.uid = uid
            self.app = shortuuid.uuid()

        def __eq__(self, other):
            return self.uid == other.uid

    route     = shortuuid.uuid()
    websocket = ws(shortuuid.uuid())

    ep = CmdTest(route=route,websocket=websocket)

    assert ep.route     == route
    assert ep.websocket == websocket
    assert ep.app       == websocket.app

def test_command_decorator():
    ep = CmdTest(route=None,websocket=None)

    assert 'foo'   in ep.commands
    assert 'bar'   in ep.commands
    assert 'fubar' in ep.commands

    assert 'nothing' not in ep.commands

    assert ep.commands['foo']   == ep.on_foo
    assert ep.commands['bar']   == ep.on_bar
    assert ep.commands['fubar'] == ep.on_bar

    assert ep.on_foo in ep.commands.values()
    assert ep.on_bar in ep.commands.values()
    assert ep.on_nothing not in ep.commands.values()

cmd_test = namedtuple('cmd_test', ['params', 'expected'])
cmd_route_tests = [
    cmd_test(params={'route': None, 'cmd': 'foo',     'data': {}}, expected={'foo': 1}),
    cmd_test(params={'route': None, 'cmd': 'bar',     'data': {}}, expected={'bar': 1}),
    cmd_test(params={'route': None, 'cmd': 'fubar',   'data': {}}, expected={'bar': 1}),
    cmd_test(params={'route': None, 'cmd': 'nothing', 'data': {}}, expected={}),
    cmd_test(params={'route': None, 'cmd': 'arg_1',   'data': {}}, expected={}),
    cmd_test(params={'route': None, 'cmd': 'arg_2',   'data': {}}, expected={}),
    cmd_test(params={'route': None, 'cmd': 'arg_opt', 'data': {}}, expected={'arg_opt': None}),

    cmd_test(params={'route': None, 'cmd': 'foo',     'data': {'arg':2}}, expected={}),
    cmd_test(params={'route': None, 'cmd': 'arg_1',   'data': {'arg9':2}}, expected={}),

    cmd_test(params={'route': None, 'cmd': 'arg_1',   'data': {'arg1':2}}, expected={'arg_1': 2}),
    cmd_test(params={'route': None, 'cmd': 'arg_opt', 'data': {'arg1':2}}, expected={'arg_opt': 2}),

    cmd_test(params={'route': None, 'cmd': 'arg_2',   'data': {'arg1':2, 'arg2':3}}, expected={'arg_2':(2,3)}),
    cmd_test(params={'route': None, 'cmd': 'arg_2',   'data': {'arg2':3, 'arg1':2}}, expected={'arg_2':(2,3)}),

    cmd_test(params={'route': None, 'cmd': 'arg_2',   'data': ['val1','val2']}, expected={}),
]

@pytest.mark.asyncio
@pytest.mark.parametrize("params,expected", cmd_route_tests)
async def test_cmd_routing(params, expected):
    ep = CmdTest(None, None)
    await ep.on_receive(**params)
    assert ep.called == expected

@pytest.mark.asyncio
async def test_cmd_connect():
    ep = CmdTest(None, None)

    return_route = 'DEADBEEF'

    await ep.on_receive(**{
        'route': 'FEEDFEFE',
        'cmd': 'connect',
        'data': {
            'route': return_route,
        }
    })

    assert ep.routeResponse == return_route

@pytest.mark.asyncio
async def test_cmd_disconnect():
    ep = CmdTest(None, None)

    return_route = 'DEADBEEF'

    disconnect = await ep.on_receive(**{
        'route': 'FEEDFEFE',
        'cmd': 'connect',
        'data': {
            'route': return_route,
        }
    })

    assert not disconnect

    disconnect = await ep.on_receive(**{
        'route': 'FEEDFEFE',
        'cmd': 'disconnect',
        'data': {
        }
    })

    assert disconnect

    assert ep.routeResponse == return_route

cmd_send_tests = [
    cmd_test(params={'route': None,         'cmd': 'foo'}, expected={'route': 'DEADBEEF',   'cmd': 'foo', 'data': {}}),
    cmd_test(params={'route': 'HelloWorld', 'cmd': 'foo'}, expected={'route': 'HelloWorld', 'cmd': 'foo', 'data': {}}),
    cmd_test(params={'route': 'HelloWorld', 'cmd': 'foo', 'arg1': 'val1'}, expected={'route': 'HelloWorld', 'cmd': 'foo', 'data': {'arg1': 'val1'}}),
]

@pytest.mark.asyncio
@pytest.mark.parametrize("params,expected", cmd_send_tests)
async def test_cmd_send(params, expected):
    class WSRecv:
        sent = None
        async def send_bytes(self, value):
            print('='*80)
            print('value')
            self.sent = orjson.loads(value.encode())

    recv = WSRecv()
    ep = CmdTest(None, recv)

    return_route = 'DEADBEEF'

    await ep.on_receive(**{
        'route': 'FEEDFEFE',
        'cmd': 'connect',
        'data': {
            'route': return_route,
        }
    })

    assert ep.routeResponse == return_route

    await ep.send_cmd(**params)

    assert recv.sent == expected

