import pathlib
import setuptools
import distutils
import os
import subprocess
import re

# Get the app version
appvars = {}
with open("wsrouter/vars.py") as fp:
    exec(fp.read(), appvars)

module_packages = setuptools.find_packages()

def read_text(filename: str):
    return open(filename).read()

setuptools.setup(
    name                 = "wsrouter",
    packages             = module_packages,
    use_scm_version      = True,
    license              = read_text("LICENSE.txt"),
    description          = "WebSocket Message Router",
    author               = "David Morris",
    author_email         = "othalan@othalan.net",
    url                  = "https://gitlab.com/othalan/wsrouter",
    include_package_data = True,
    zip_safe             = False,
    python_requires      = '>=3.10',
    classifiers = [
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
        "Operating System :: MacOS :: MacOS X",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
    ],
    setup_requires = [
        'setuptools_scm'
    ],
    install_requires = [
        "setuptools",
        "orjson",
        "starlette",
    ],
    extras_require = {
        "test": [
            "uvicorn[standard]",
            "coveralls",
            "pytest",
            "pytest-asyncio",
            "pytest-clarity",
            "pytest-cov",
            "pytest-mock",
            "pytest-sugar",
            "pyfakefs",
        ],
    },
)
