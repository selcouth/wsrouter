# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------
import os
import sys
from pathlib import Path
sys.path.insert(0, Path(__file__).parent.parent.parent)

# -- Project information -----------------------------------------------------

project = 'wsrouter'
author = 'David Morris'

# The full version, including alpha/beta/rc tags
# ... obtained from poetry
# ... which is our definitive version source during build stages
import subprocess
vproc = subprocess.run(['poetry', 'version', '-s'], capture_output=True)
release = vproc.stdout.decode().strip()
print(f"RELEASE: {release}")

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    #'sphinx.ext.autodoc',  # Core Sphinx library for auto html doc generation from docstrings
    #'sphinx.ext.autosummary',  # Create neat summary tables for modules/classes/methods etc
    'sphinx.ext.viewcode',  # Add a link to the Python source code for classes, functions etc.
    #'sphinx_autodoc_typehints', # Automatically document param types (less noise in class signature)
    'sphinx.ext.intersphinx',  # Link to other project's documentation (see mapping below)
    'sphinx_immaterial', # Sphinx Immaterial Theme
    'sphinx_immaterial.apidoc.python.apigen', # Sphinx Python AutoDoc
]

# Mappings for sphinx.ext.intersphinx. Projects have to have Sphinx-generated doc! (.inv file)
intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
    "poetry": ("https://python-poetry.org/", None),
}
autosummary_generate = True  # Turn on sphinx.ext.autosummary
autoclass_content = "both"  # Add __init__ doc (ie. params) to class summaries
html_show_sourcelink = False  # Remove 'view source code' from top of page (for html, not python)
autodoc_inherit_docstrings = True  # If no docstring, inherit from base class
#autodoc_typehints = "description" # Sphinx-native method. Not as good as sphinx_autodoc_typehints
set_type_checking_flag = True  # Enable 'expensive' imports for sphinx_autodoc_typehints
add_module_names = False # Remove namespaces from class/method signatures

# Add any paths that contain templates here, relative to this directory.
templates_path = ['templates']

# -- Options for HTML output -------------------------------------------------

# Readthedocs theme
# on_rtd is whether on readthedocs.org, this line of code grabbed from docs.readthedocs.org...

html_theme = "sphinx_immaterial"
html_theme_options = {
    "repo_url": "https://gitlab.com/selcouth/wsrouter/",
    "repo_name": "wsrouter",
    "repo_type": "gitlab",
    "icon": {
        "repo": "fontawesome/brands/gitlab",
    },
    "edit_uri": "blob/main/docs/source",
    "palette": [
        {
            "media": "(prefers-color-scheme: dark)",
            "scheme": "slate",
            "primary": "green",
            "accent": "lime",
            "toggle": {
                "icon": "material/lightbulb",
                "name": "Switch to light mode",
            },
        },
        {
            "media": "(prefers-color-scheme: light)",
            "scheme": "default",
            "primary": "light-green",
            "accent": "light-blue",
            "toggle": {
                "icon": "material/lightbulb-outline",
                "name": "Switch to dark mode",
            },
        },
    ],

    "globaltoc_collapse": False,
    "version_dropdown": False,
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['static']

# Python Customization
python_apigen_modules = {
    "wsrouter": "autoapi/api",
}

python_resolve_unqualified_typing = True
python_transform_type_annotations_pep585 = True
python_transform_type_annotations_pep604 = True

python_apigen_default_groups = [
    ("class:.*", "Classes"),
    ("method:.*", "Methods"),
    ("property:.*", "Properties"),
    (r".*:.*\.__[a-z0-9_]*__", "Special Methods"),
    (r".*:.*\.__(init|new)__", "Constructors"),
    (r".*:.*\.__(str|repr)__", "String representation"),
]
