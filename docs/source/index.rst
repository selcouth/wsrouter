.. toctree::
   :hidden:
   :maxdepth: 99

   Home page <self>
   API reference <apiref>


Welcome to wsrouter documentation
=================================

This package provides a Starlette WebSocket endpoint for sharing a single
websocket connection between multiple client and server endpoints.

Installation
============

Install using pip:

.. code-block:: bash

   pip install wsrouter

Install direct from source using `Poetry <https://python-poetry.org/docs/>`_.

.. code-block:: bash

   # Install poetry (needed for build and test)
   pip install poetry

   # Install wsrouter
   git clone https://gitlab.com/selcouth/wsrouter.git
   cd wsrouter
   poetry install

   # Run tests
   poetry run pytest


Basic Usage
===========

Python Code:

.. code-block:: python

    from starlette.applications import Starlette
    from starlette.routing import WebSocketRoute
    from starlette.routing import Mount
    from wsrouter import WebSocketRouter
    from wsrouter import WebSocketCommandEndpont
    from wsrouter import command

    class WSRoute1(WebSocketCommandEndpont):
        @command("cmd_1")
        async def on_cmd_1(self, param1, param2, _rID)
            # NOTE: _rID is added automatically by javascript code in getCmdResponse()
            # ... This is a unique response route identifier
            # ... This permits sending data to a specific command
            assert param1 == 'hello'
            assert param2 == 'world'
            await self.send_cmd(
                _rID,
                reply = f"You Sent: {param1} {param2}",
            )

        @command("cmd_2")
        async def on_cmd_2(self):
            await self.send_cmd(
                'response_2',
                text = "hello world!")

    wsroutes = {
        "/ws_route_1": WSRoute1,
    }

    routes = [
        WebSocketRoute("/ws",  endpoint = WebSocketRouter(routes=wsroutes)),
        Mount('/js/websocket', app = StaticFiles(directory = WebSocketRouter.JS_PATH), name = 'jsws'),
    ]

    app = Starlette(debug=True, routes=routes, lifespan=lifespan)

Javascript Code:

.. code-block:: javascript

    import { WSock} from './websocket/websocket.js'

    const socket = new WSock('/ws');
    const py_route = "/ws_route_1"

    function onWSDataReceived({cmd, data}) {
        switch(cmd) {
            default:
                console.log(`[${py_route}] Command Not Recognized: ${cmd}`);
        }
    }

    function onResponse2({ data }) {
        console.log(`Response: ${data.text}0;`
    }

    // Connect the route and retrieve a session unique routeid code
    // This MUST be called first!
    const routeid = socket.getRoute(
        py_route,         // The route setup in Python
        onWsDataReceived) // The default callback for messages on this socket

    // Setup a new command handler
    socket.setCmdHandler(routeid, "response_2", onResponse2)

    // Send a command to the server python code
    socket.sendCmd(py_route, "cmd_2", {})

    // Get a one-time callback response from the server:
    socket.getCmdResponse(
        py_route,
        routeid,
        "cmd_1",
        {
            param1: 'hello', // NOTE the use of 'param1' and 'param2'
            param2: 'world', // These MUST match the function argument names in python for this command
        }
    ).then(({data}) => console.log("RECEIVED:", data.reply));
