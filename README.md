# WebSocket Router for Starlette

This package acts as a websocket message router for [Starlette](https://github.com/encode/starlette)
[WebSocket](https://www.starlette.io/websockets/) connections, permitting socket sharing for
multiple client-server connections.

For installation and usage, [see the documentation](https://selcouth.gitlab.io/wsrouter).
